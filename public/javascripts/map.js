var map = L.map("main_map").setView([-35.1861, -59.0951], 15);

L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
    attribution:
        '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMaps</a> contributors',
}).addTo(map);

//L.marker([-35.18581, -59.09774]).addTo(map)
//L.marker([-35.18403, -59.09684]).addTo(map)

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function (result) {
        console.log(result);
        result.bicicleta.forEach(function (bici) {
            L.marker(bici.ubicacion, { title: bici.id }).addTo(map);
        });
    },
});
