var Bicicleta = require("../../models/bicicleta");
var mongoose = require("mongoose");

describe("Testing Bicicletas", function () {
  beforeEach(function (done) {
    var mongoDB = "mongodb://localhost/testdb";
    mongoose.connect(mongoDB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    const db = mongoose.connection;
    db.on("error", console.error.bind(console, "Connection error"));
    db.once("open", function () {
      console.log("Test database is connected");
    });
    done();
  });

  afterEach(function (done) {
    Bicicleta.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      done();
    });
  });
  describe("Bicicleta.createInstance", () => {
    it("crea una instancia de Bicicleta", () => {
      var bici = Bicicleta.createInstance(1, "verde", "urbana", [
        -35.1865,
        -59.0984,
      ]);

      expect(bici.code).toBe(1);
      expect(bici.color).toBe("verde");
      expect(bici.ubicacion[0]).toEqual(-35.1865);
      expect(bici.ubicacion[1]).toEqual(-59.0984);
    });
  });

  describe("Bicicletas.allBicis", () => {
    it("comienza vacia", (done) => {
      Bicicleta.allBicis(function (err, bicis) {
        expect(bicis.length).toBe(0);
        done();
      });
    });
  });

  describe("Bicicleta.add", () => {
    it("agrega una bicicleta", (done) => {
      var aBici = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });
      Bicicleta.add(aBici, function (err, newBici) {
        if (err) console.log(err);
        Bicicleta.allBicis(function (err, bicis) {
          expect(bicis.length).toEqual(1);
          expect(bicis[0].code).toEqual(aBici.code);
          done();
        });
      });
    });
  });

  describe("Bicicleta.findByCode", () => {
    it("debe devolver la bici con code 1", (done) => {
      Bicicleta.allBicis(function (err, bicis) {
        expect(bicis.length).toBe(0);

        var aBici = new Bicicleta({
          code: 1,
          color: "verde",
          modelo: "urbana",
        });
        Bicicleta.add(aBici, function (err, newBici) {
          if (err) console.log(err);

          var aBici2 = new Bicicleta({
            code: 2,
            color: "rojo",
            modelo: "montania",
          });
          Bicicleta.add(aBici2, function (err, newBici) {
            if (err) console.log(err);
            Bicicleta.findByCode(1, function (error, targetBici) {
              expect(targetBici.code).toBe(aBici.code);
              expect(targetBici.color).toBe(aBici.color);
              expect(targetBici.modelo).toBe(aBici.modelo);

              done();
            });
          });
        });
      });
    });
  });
});

/* beforeEach(() => {
    Bicicleta.allBicis = []
})

describe('Bicicleta.allBicis', () => {
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0)
    })
})

describe('Bicicleta.add', () => {
    it('agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0)

        var a = new Bicicleta(1, 'rojo', 'urbana', [-35.18495, -59.09779])
        Bicicleta.add(a)

        expect(Bicicleta.allBicis.length).toBe(1)
        expect(Bicicleta.allBicis[0]).toBe(a)
    })
})

describe('Bicicleta.findById', () => {
    it('debe devolver la bici con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0)

        var aBici = new Bicicleta(1, "verde", "urbana")
        Bicicleta.add(aBici)

        var targetBici = Bicicleta.findById(1)
        expect(targetBici.id).toBe(aBici.id)
        expect(targetBici.color).toBe(aBici.color)
        expect(targetBici.modelo).toBe(aBici.modelo)
    })
})

describe('Bicicleta.removeById', () => {
    it('debe remover la bicicleta con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0)

        var aBici = new Bicicleta(1, "verde", "urbana")
        Bicicleta.add(aBici)
        expect(Bicicleta.allBicis.length).toBe(1)
        var targetBici = Bicicleta.removeById(1)
        expect(Bicicleta.allBicis.length).toBe(0)
    })
}) */
