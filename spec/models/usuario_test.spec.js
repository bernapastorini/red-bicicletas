const mongoose = require("mongoose");
const Bicicleta = require("../../models/bicicleta");
const Usuario = require("../../models/usuario");
const Reserva = require("../../models/reserva");

describe("Testing Usuarios", function () {
  beforeEach(function (done) {
    const mongoDB = "mongodb://localhost/testdb";
    mongoose.connect(mongoDB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    const db = mongoose.connection;

    db.on("error", console.error.bind(console, "Connection error"));
    db.once("open", function () {
      console.log("Connected to DB");
      done();
    });
  });

  afterEach(function (done) {
    Reserva.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      Usuario.deleteMany({}, function (err, success) {
        if (err) console.log(err);
        Bicicleta.deleteMany({}, function (err, success) {
          if (err) console.log(err);
          done();
        });
      });
    });
  });

  describe("Cuando un usuario reserva una bici", () => {
    it("debe existir la reserva", (done) => {
      const usuario = new Usuario({ nombre: "Bernardo" });
      usuario.save();
      const bicicleta = new Bicicleta({
        code: 1,
        color: "verde",
        modelo: "urbana",
      });
      bicicleta.save();

      var hoy = new Date();
      var maniana = new Date();

      maniana.setDate(hoy.getDate() + 1);
      usuario.reservar(bicicleta.id, hoy, maniana, function (err, reserva) {
        Reserva.find({})
          .populate("bicicleta")
          .populate("usuario")
          .exec(function (err, reservas) {
            console.log(reservas[0]);
            expect(reservas.length).toBe(1);
            expect(reservas[0].diasDeReserva()).toBe(2);
            expect(reservas[0].bicicleta.code).toBe(1);
            expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
            done();
          });
      });
    });
  });
});
