var Bicicleta = require("../../models/bicicleta");
var request = require("request");
var server = require("../../bin/www");
var mongoose = require("mongoose");
const { base } = require("../../models/bicicleta");

const base_url = "http://localhost:5000/api/bicicletas";

describe("Bicicleta API", () => {
  beforeEach(function (done) {
    var mongoDB = "mongodb://localhost/testdb";
    mongoose.connect(mongoDB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    const db = mongoose.connection;
    db.on("error", console.error.bind(console, "Connection error"));
    db.once("open", function () {
      console.log("Connected to DB");
      done();
    });
  });

  afterEach(function (done) {
    Bicicleta.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      done();
    });
  });

  describe("GET BICICLETAS /", () => {
    it("Status 200", (done) => {
      request.get(base_url, function (error, response, body) {
        var result = JSON.parse(body);
        expect(response.statusCode).toBe(200);
        expect(result.bicicletas.length).toBe(0);
        done();
      });
    });
  });

  describe("POST BICICLETAS /create", () => {
    it("STATUS 200", (done) => {
      var headers = { "content-type": "application/json" };
      var aBici =
        '{"code": 10, "color": "rojo", "modelo": "urbana", "lat": -35.1866, "long": -59.0984';
      request.post(
        {
          headers: headers,
          url: base_url + "/create",
          body: aBici,
        },
        function (error, response, body) {
          expect(response.statusCode).toBe(200);
          var bici = JSON.parse(body).bicicleta;
          console.log(bici);
          expect(bici.color).toBe("rojo");
          expect(bici.ubicacion[0]).toBe(-35.1866);
          expect(bici.ubicacion[1]).toBe(-59.0984);
          done();
        }
      );
    });
  });

/*   describe("POST BICICLETAS /update", () => {
    it("STATUS 200", (done) => {
      Bicicleta.allBicis = [];
      expect(Bicicleta.allBicis.length).toBe(0);
      var headers = { "content-type": "application/json" };
      var aBici =
        '{"id": 4, "color": "azul", "modelo": "mountain", "lat": -35, "lng": -59}';

      request.post(
        {
          headers: headers,
          url: "http://127.0.0.1:3000/api/bicicletas/create",
          body: aBici,
        },
        function (error, response, body) {
          expect(response.statusCode).toBe(200);
          expect(Bicicleta.allBicis.length).toBe(1);
          expect(Bicicleta.findById(4).color).toBe("azul");
          done();
        }
      );
    });

    it("STATUS 200", (done) => {
      var headers = { "content-type": "application/json" };
      var bBici =
        '{"id": 4, "color": "verde", "modelo": "carrera", "lat": -35.18632, "lng": -59.09836}';
      request.post(
        {
          headers: headers,
          url: "http://127.0.0.1:3000/api/bicicletas/update",
          body: bBici,
        },
        function (error, response, body) {
          expect(response.statusCode).toBe(200);
          expect(Bicicleta.allBicis.length).toBe(1);
          expect(Bicicleta.findById(4).color).toBe("verde");
          done();
        }
      );
    });
  }); */
});
